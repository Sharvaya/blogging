import "./Home.scss";


const Home = () => {

    return <div>
        <Banner/>
        <div className="main-content">
            <div className="layout">
            <Category/>
            <Products  headingtext="Popular Products"/>
           
            </div>
        </div>
        
    </div>
};

export default Home;
